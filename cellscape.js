define(['util/API', 'site/siteAIRecords'], function(API, siteAIRecords) {
    'use strict';

    var onSaveCustomMethod = function(individual) {
        debugger;
        // SAMPLE CODE
        
		if (individual.get('kbClassId') == 'cell:ClarityID') {
            individual.set('sys:name', "CS");        
        }
        if (individual.get('kbClassId') == 'cell:A10100BloodSampleAccession') {
			individual.set('sys:name', 'CS');
            /*
			if (individual.get('cell:hasClarityID')[0] == null) {
                alert('You must specify a Clarity ID');
                return false;			
            }    else {
                individual.set('sys:name', individual.get('cell:hasClarityID')[0].id);
            
            }
			//*/			
        } 
        if (individual.get('kbClassId') == 'cell:A10200BloodSamplePreProcessing') {
            if (window.verified == undefined) {
                var app_ind_id = individual.get('cell:hasBloodSampleAccession')[0];
                _.defer(function() {
                     API.fetchObjectById({id:app_ind_id})
                        .done(function(response) {
                            window.verified = true;
                            individual.set('sys:name', response.data['sys:uniqueId'][0]);
                            $('a.save').click();
                        })
                });
                return false;
            }
            else {
                window.verified = undefined;
            }
        }
        if (individual.get('kbClassId') == 'cell:zPlannedDeviation') {
            if (individual.get('cell:pdNum')[0] == null) {
                alert('You must specify a PD Number');
                return false;			
            }    else {
                individual.set('sys:name', "PD-"+individual.get('cell:pdNum')[0]);            
            }
        }        
    }
    
    return {
        /**
             * This hook can be used to add a site-wide custom menu to the top of the page.
             * @param options an object with the following properties/methods:
             *                - render a method which renders a custom menu on the page; this
             *                         method takes two arguments:
             *                         - [0] name:  the custom menu's name
             *                         - [1] items: an array of object literals which represent the
             *                                      custom menu's options; each item should have a
             *                                      "name" and a "url" property, which will become
             *                                      the menu option's label and link address
             */
        pageHeader: {
        },
        rtObjectCreate: {
            /**
             * This hook can be used to add additional validation checks or post-processing when a
             * new Syapse object is saved.  
             * @param individual  the individual being saved
             * @returns {Boolean} if undefined or true, saving of the object will continue as
             *                    normal; if false is returned saving will be prevented 
             */
            onSave: onSaveCustomMethod,
            postIndividualInitialize: function(individual) {
                //* SAMPLE CODE 
                debugger;
                _.delay(function() {$('div.form-scope').find('div[name="Basic"]').next().hide()}, 500);
                if (individual.get('kbClassId') == 'cell:A10100BloodSampleAccession') {
                    individual.set('sys:name', 'CS');
                }
                if (individual.get('kbClassId') == 'cell:A10200BloodSamplePreProcessing') {
                    individual.set('sys:name', '[will autopopulate with Blood Sample ID]');
                }              
                if (individual.get('kbClassId') == 'cell:zPlannedDeviation') {
                    individual.set('sys:name', '[will autopopulate with PD Number]');
                }
                if (individual.get('kbClassId') == 'cell:CellGroup') {
                    _.defer(function() {
                    $('div.form-scope').find('div[name="Purity"]').next().hide()
                    $('div.form-scope').find('div[name="Quantitation"]').next().hide()
                    });//,250);
                }
                if (individual.get('kbClassId') == 'cell:A20500ACGHSample') {
                    _.defer(function() {
                    $('div.form-scope').find('div[name="Labeling"]').next().hide()
                    $('div.form-scope').find('div[name="Quantitation"]').next().hide()
                    $('div.form-scope').find('div[name="Purification"]').next().hide()
                    $('div.form-scope').find('div[name="Hybridization"]').next().hide()
                    });//,250);
                }
			}
			    //*/
        },
        rtObjectEdit: {
            /**
             * This hook can be used to add additional validation checks or post-processing when an
             * existing Syapse object is saved.  
             * @param individual  the individual being saved
             * @returns {Boolean} if undefined or true, saving of the object will continue as
             *                    normal; if false is returned saving will be prevented 
             */
            onSave: onSaveCustomMethod,
            postIndividualInitialize: function(individual) {
                //* SAMPLE CODE 
                debugger;
                _.defer(function() {$('div.form-scope').find('div[name="Basic"]').next().hide()});
                if (individual.get('kbClassId') == 'cell:A10100BloodSampleAccession') {
                    individual.set('sys:name', 'CS');
                }
                if (individual.get('kbClassId') == 'cell:CellGroup') {
                    _.defer(function() {
                    $('div.form-scope').find('div[name="Purity"]').next().hide()
                    $('div.form-scope').find('div[name="Quantitation"]').next().hide()
                    });//,250);
                }
                if (individual.get('kbClassId') == 'cell:A20500ACGHSample') {
                    _.defer(function() {
                    $('div.form-scope').find('div[name="Labeling"]').next().hide()
                    $('div.form-scope').find('div[name="Quantitation"]').next().hide()
                    $('div.form-scope').find('div[name="Purification"]').next().hide()
                    $('div.form-scope').find('div[name="Hybridization"]').next().hide()
                    });//,250);
                }
			}
			    //*/
        },
        rtObjectDisplay: {
            /**
             * This hook can be used to add an additional "custom" section to the bottom of the
             * Syapse object display page.
             * @param options an object with various properties (and a single method) which
             *                can be used to help build the custom section.  The
             *                properties/method of this object are:
             *                - airecord:        a raw JSON representation of the page's Individual
             *                - class_label:     the human-readable name of the (knowledge base)
             *                                   class of the page's Individual
             *                - container:       the page's HTML element 
             *                - object_name:     the name of the page's Individual
             *                - object_uniqueID: the Unique ID (if any) of the page's Individual
             *                - page_url:        the URL of the page
             *                - render:          this method can be used to render the section
             *                                   itself; it takes two arguments:
             *                                   - [0] title: the section's title
             *                                   - [1] content: the section's HTML content
             */
            addCustomSection: function(customSection) {
                //*
                var title = 'CellScape';
                var content =  "<img src=\"http://www.cellscapecorp.com/wp-content/themes/expresso/images/logo.png\" alt=\"CellScape\" >"  
                customSection.render(title, content, customSection);
                //*/
            },
            /**
             * This hook can be used to add additional menu items to the Syapse object display page.
             * @param options an object with the following properties/methods:
             *                - render a method which renders a custom menu on the page; this
             *                         method takes one argument, which should be an array of menu
             *                         item JS object literals.  These objects should have 2, or
             *                         optionally 3 properties:
             *                         cssClass: (optional) class or classes to add to the menu item
             *                         href:     the URL the menu item will navigate to when clicked
             *                         name:     the visible text of the menu item
             */
            addMenuItems: function(options) {
                /*
                options.render([{cssClass: "cust", href: '/customUrl', name: "Custom menu 1"},
                                      {cssClass: "cust", href: '/customUrl2', name: "Custom menu 2"}]);
                */
            },
        },
        rtSyObjectList: {
            /**
             * This hook can be used to add additional menu items to the Syapse object list page.
             * @param options an object with the following properties/methods:
             *                - render a method which renders a custom menu on the page; this
             *                         method takes one argument, which should be an array of menu
             *                         item JS object literals.  These objects should have 2, or
             *                         optionally 3 properties:
             *                         cssClass: (optional) class or classes to add to the menu item
             *                         href:     the URL the menu item will navigate to when clicked
             *                         name:     the visible text of the menu item
             */
            addMenuItems: function(options) {
                /*
                options.render([{cssClass: "cust", href: '/customUrl', name: "Custom menu 1"},
                                      {cssClass: "cust", href: '/customUrl2', name: "Custom menu 2"}]);
                */
            },
        }
    };
});